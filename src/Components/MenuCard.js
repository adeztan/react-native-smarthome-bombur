import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from "../resources/Colors";
import {FontsWeight, Fonts} from "../resources/Fonts";
import UIConst from "../resources/Const";
import PropTypes from "prop-types";

export default class MenuCard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity
                onPress={!this.props.disabled ? this.props.onPress : null}
                style={styles.functionButton}>
                <View style={styles.buttonButtonWrp}>
                    <Image
                        style={styles.functionButtonWin}
                        source={this.props.image}/>
                    <Text style={[styles.functionName, this.props.disabled && styles.functionNameDisabled]}>{this.props.title }</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

MenuCard.propTypes = {
    onPress: PropTypes.func,
    image: PropTypes.any,
    title: PropTypes.string,
    disabled: PropTypes.bool
};

const styles = StyleSheet.create({
    functionButton: {
        // width: (UIConst.deviceWidth / 3) - 10,
        flex: 1,
        margin: 4,
        borderRadius: 2,
        backgroundColor: Colors.white,
        padding: 8,
        ...UIConst.shadow2pt
    },
    buttonButtonWrp: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    functionButtonWin: {
        width: 64,
        height: 64,
        resizeMode: 'contain'
        // marginTop: 2.5,
    },
    functionName: {
        ...FontsWeight.Regular,
        fontSize: 12,
        color: '#263238',
        textAlign: 'center',
        marginTop: 4,
        lineHeight: 18
    },
    functionNameDisabled: {
        color: Colors.gray
    }
});