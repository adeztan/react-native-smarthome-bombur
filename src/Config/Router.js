import React from 'react';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';

import Home from "../App/Home";
import Profile from "../App/Profile";
import Setting from "../App/Settings";
import History  from "../App/History"

export const TabBottomNavigation = createMaterialBottomTabNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions:{
                tabBarLabel:'Home',
                tabBarIcon: ({tintColor}) => (
                    <Icon size={25} name={'home'} color={tintColor}/>
                ),
                barStyle: { backgroundColor: '#8f1537', fontWeight: 'bold' },
                activeColor: '#ffffff',
                inactiveColor: '#d4d7dd',
            },
        },
        History: {
            screen: History,
            navigationOptions:{
                tabBarLabel:'History',
                tabBarIcon: ({tintColor}) => (
                    <Icon size={25} name={'history'} color={tintColor}/>
                ),
                barStyle: { backgroundColor: '#8f1537', fontWeight: 'bold' },
                activeColor: '#ffffff',
                inactiveColor: '#d4d7dd',
            },
        },
        Profile: {
            screen: Profile,
            navigationOptions:{
                tabBarLabel:'Profile',
                tabBarIcon: ({tintColor}) => (
                    <Icon size={25} name={'id-badge'} color={tintColor}/>
                ),
                barStyle: { backgroundColor: '#8f1537', fontWeight: 'bold' },
                activeColor: '#ffffff',
                inactiveColor: '#d4d7dd',
            },
        },


    },
    {
        tabBarOptions: {
            showIcon: true,
        },
    }
);
