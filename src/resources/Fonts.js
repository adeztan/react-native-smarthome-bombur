import {Platform} from 'react-native';

const fontType = {
    // IOS
    ronnia: 'Ronnia',

    // Android
    ronniaLight: 'RonniaLight',
    ronniaBold: 'RonniaBold',
    ronniaReg: 'RonniaReg',
    ronniaExtrabold: 'RonniaExtrabold',

    robotoRegular: 'Roboto-Regular',
    robotoMedium: 'Roboto-Medium',
    robotoBold: 'Roboto-Bold',
};


const FontsWeight = {
    Light: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: '300',
            backgroundColor: 'transparent',
            paddingTop: 3
        },
        android: {
            fontFamily: fontType.ronniaLight,
        }
    }),
    Regular: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: 'normal',
            backgroundColor: 'transparent',
            paddingTop: 3
        },
        android: {
            fontFamily: fontType.ronniaReg,
        }
    }),
    SemiBold: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: '500',
            backgroundColor: 'transparent',
            paddingTop: 3
        },
        android: {
            fontFamily: fontType.ronniaReg
        }
    }),
    Bold: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: '700',
            backgroundColor: 'transparent',
            paddingTop: 3
        },
        android: {
            fontFamily: fontType.ronniaBold
        }
    }),
    RobotoRegular: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: 'normal',
            backgroundColor: 'transparent',
            paddingTop: 3
        },
        android: {
            fontFamily: fontType.robotoRegular
        }
    }),
    RobotoSemiBold: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: '500',
            backgroundColor: 'transparent',
            paddingTop: 3
        },
        android: {
            fontFamily: fontType.robotoMedium
        }
    }),
    RobotoBold: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: '700',
            backgroundColor: 'transparent',
            paddingTop: 3
        },
        android: {
            fontFamily: fontType.robotoBold
        }
    })
};

const TextInputFont = {
    Regular: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: 'normal',
            backgroundColor: 'transparent',
            marginTop: 3
        },
        android: {
            fontFamily: fontType.ronniaReg,
        }
    }),
    Bold: Platform.select({
        ios: {
            fontFamily: fontType.ronnia,
            fontWeight: '700',
            backgroundColor: 'transparent',
            marginTop: 3
        },
        android: {
            fontFamily: fontType.ronniaBold
        }
    })
};

const Fonts = {
    h2: {
        ...FontsWeight.Bold,
        fontSize: 28
    },
    h3: {
        ...FontsWeight.Bold,
        fontSize: 24
    },
    h4: {
        ...FontsWeight.Bold,
        fontSize: 18,
        lineHeight: 20,
    },
    h5: {
        ...FontsWeight.Bold,
        fontSize: 14,
    },
    h6: {
        ...FontsWeight.Bold,
        fontSize: 20,
        letterSpacing: 0.15,
    },
    p: {
        ...FontsWeight.Regular,
        fontSize: 14,
        lineHeight: 18
    },
    hint: {
        ...FontsWeight.Regular,
        fontSize: 12,
        lineHeight: 14
    },

    listTitle: {
        ...FontsWeight.Bold,
        fontSize: 16
    },


    subtitle1: {
        ...FontsWeight.Regular,
        fontSize: 16,
        letterSpacing: 0.15,
        color: 'black'
    },

    subtitle2: {
        ...FontsWeight.Bold,
        fontSize: 14,
        letterSpacing: 0.1,
        color: 'black'
    },

    button: {
        ...FontsWeight.Bold,
        fontSize: 14,
        // letterSpacing: 0.75,
        color: 'black'
    },
};

export {
    FontsWeight,
    Fonts,
    TextInputFont
};