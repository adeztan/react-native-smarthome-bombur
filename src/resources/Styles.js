import {Platform, StyleSheet} from 'react-native';
import UIConst from "./Const";
import Colors from "./Colors";
import {FontsWeight} from "./Fonts";

const fullContainer = {
    flex: 1,
    width: null,
    height: null,
};

const Styles = {
    container: {
        ...fullContainer,
        paddingTop: UIConst.statusBarHeight,
        backgroundColor: Colors.white
    },
    fullyContainer: {
        ...fullContainer,
        backgroundColor: Colors.white
    },
    containerNoMargin: {
        ...fullContainer,
        zIndex: 4,
        flex: 1
    },
    contentContainer: {
        ...fullContainer,
        backgroundColor: Colors.semiWhite
    },
    contentContainerWhite: {
        ...fullContainer,
        backgroundColor: Colors.white
    },
    contentContainerGray: {
        ...fullContainer,
        backgroundColor: Colors.semiGray
    },
    scrollContainer: Platform.select({
        ios: {
            flex: 1,
            height: null,
            paddingTop: 30,
            paddingBottom: 30
        },
        android: {
            flex: 1,
            height: null,
            paddingVertical: 8,
            backgroundColor: Colors.white
        }
    }),
    settingsContainer: Platform.select({
        ios: {
            ...fullContainer,
            backgroundColor: Colors.settingsColor
        },
        android: {
            ...fullContainer,
            backgroundColor: Colors.white
        }
    }),
    settingScrollContainer: Platform.select({
        ios: {
            flex: 1,
            height: null,
            paddingTop: 20,
            paddingBottom: 20
        },
        android: {
            flex: 1,
            height: null,
            padding: 16,
            backgroundColor: Colors.white
        }
    }),
    settingScrollContainerWithButton: Platform.select({
        ios: {
            flex: 1,
            height: null,
            paddingTop: 20,
            marginBottom: 40,
            // paddingHorizontal: 16,
        },
        android: {
            flex: 1,
            height: null,
            paddingHorizontal: 16,
            paddingTop: 16,
            paddingBottom: 16,
            marginBottom: 40,
            backgroundColor: Colors.white
        }
    }),
    settingInnerContainer: {
        flex: 1,
        height: null,
        marginBottom: 20,
    },
    scrollContainerWithPading: {
        flex: 1,
        height: null,
        padding: 16
    },
    scrollContainerWithPadingWithButton: {
        flex: 1,
        backgroundColor: Colors.white,
        height: null,
        paddingHorizontal: 16,
        paddingTop: 16,
        paddingBottom: 52
    },
    scrollContainerWithoutPaddingWithButton: Platform.select({
        ios: {
            flex: 1,
            height: null,
            marginBottom: 40,
        },
        android: {
            flex: 1,
            height: null,
            marginBottom: 40,
            backgroundColor: Colors.white
        }
    }),
    confirmationWrapper: {
        flex: 1,
        marginVertical: 16,

        borderTopWidth: StyleSheet.hairlineWidth,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderTopColor: Colors.primaryColor,
        borderBottomColor: Colors.primaryColor
    },


    /**
     * Region Header Style
     * */
    headerStyles: {
        ...FontsWeight.Bold
    },
    navigationRightHeader: {
        paddingRight: 12,
    },
    navigationRightButton: Platform.select({
        ios: {
            marginVertical: 4,
            marginRight: 8
        },
        android: {
            marginVertical: 6,
            paddingRight: 16,
        }
    }),
    navigationRightText: Platform.select({
        ios: {
            fontSize: 16,
            color: Colors.white
        },
        android: {

        }
    }),

    row: {
        flex: 1,
        flexDirection: "row"
    },
    rowNoFlex: {
        flexDirection: 'row'
    },

    // Column
    col1: {
        flex: 1,
    },
    col2: {
        flex: 2
    },
    col3: {
        flex: 3,
    },
    noCol: {
        flex: -1
    },
    pushRight: {
        alignItems: 'flex-end',
        textAlign: 'right'
    },
    pushRightNoText: {
        alignItems: 'flex-end',
    },
    colCenter: {
        height: null,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    // End Column

    activityIndicatorWrapper: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },

    /// Saldo Component
    emasMain: {
        emasWrapper: {
            flex: 1,
            marginRight: 4
        },
        emasJudul: {
            ...FontsWeight.Light,
            color: Colors.gray
        },
        emasSaldo: {
            ...FontsWeight.Regular,
            fontSize: 20,
            color: Colors.emas,
            marginVertical: 4
        },
        emasBeli: {
            ...FontsWeight.Regular,
            fontSize: 20,
            color: Colors.primaryColor,
            marginVertical: 4
        },
        emasJual: {
            ...FontsWeight.Regular,
            fontSize: 20,
            color: Colors.merah500,
            marginVertical: 4
        },
        tangalBerlaku: {
            ...FontsWeight.Light,
            color: Colors.gray,
            marginTop: 4
        },
        hargaWrapper: {
            flexDirection: "row",
            flex: 1
        },
        emasRefreshWrapper: {
            flexDirection: 'row'
        },
        emasRefreshWrapper5S: {
            flexDirection: 'column'
        },

        emasJudulWrapper: {
            flex: -1,
            marginRight: 4
        },

        refreshWrapper: {
            flex: -1,
            height: null,
            justifyContent: 'center'
        },
        buttonRefreshWrapper: {
            width: 24,
            height: 24,
            backgroundColor: Colors.emas,
            borderRadius: 12,
            alignItems: 'center',
            justifyContent: 'center'
        }
    },

    emptyCard: {
        flex: 1,
        margin: 12
    },

    iOSPhotoContainer: Platform.select({
        ios: {
            backgroundColor: Colors.white,
            paddingHorizontal: 16,
            paddingTop: 2,
            paddingBottom: 4,
            borderColor: Colors.gray,
            borderTopWidth: StyleSheet.hairlineWidth,
            borderBottomWidth: StyleSheet.hairlineWidth
        }
    }),

    // region Fake Navigation
    androidHeaderContainer: {
        width: null,
        height: 56,
        paddingVertical: 12,
        paddingHorizontal: 16,
        backgroundColor: Colors.primaryColor,
        ...UIConst.shadow4pt,
        flexDirection: 'row'
    },
    innerHeaderContainer: {
        flex: 1,
        height: null,
        justifyContent: 'center',
        borderRadius: 2,
        marginRight: 24,
        paddingHorizontal: 4,
    },
    iconContainer: Platform.select({
        android: {
            marginVertical: 4,
        },
        ios: {
            marginVertical: 4
        }
    }),

    marginRight24: {
        marginRight: 24
    },
    iconInnerContainer: Platform.select({
        android: {
            padding: 2
        },
        ios: {
            padding: 2
        }
    }),
    // endregion

    // region FontStyling
    fontRegular: {
        ...FontsWeight.Regular,
        marginBottom: 2
    },
    fontBold: {
        ...FontsWeight.Bold,
        marginBottom: 2
    },
    fontRegBold: {
        ...FontsWeight.Regular,
        fontWeight: '700',
        marginBottom: 2
    },
    fontBlack: {
        color: Colors.black,
    },
    fontWhite: {
        color: Colors.white
    },
    fontPrimaryDark: {
        color: Colors.primaryColorDark
    },
    fontPrimary: {
        color: Colors.primaryColor
    },
    fontAlignCenter: {
        textAlign: 'center'
    },
    /**
     * function Style untuk berapa besar font, supaya tidak banyak deklarasi
     * @param {number} size Ukuran font
     * @return {object} Style object
     */
    fontSize: function(size) {
        return {
            fontSize: size
        }
    },
    /**
     * function styles untunk menentukan berapa line height yang dibutuhkan
     * @param {number} height Line Height yang diinginkan referensi iOS (14 default untuk 14 pt)
     * @return {object} style object
     */
    fontLineHeight: function(height = 14) {
        return {
            lineHeight: Platform.OS === 'ios' ? height : (height + (height / 2)),
        }
    },
    // endregoin

    // region GCash
    GCash: {
        containerListWrapper: {
            padding: 16,
            marginBottom: 24,
            backgroundColor: Colors.white
        },
        imageIcon: {
            width: 100,
            height: 48,
            resizeMode: 'contain',
            marginBottom: 8,
        },
        imageWrapper: {
            borderBottomWidth: 1,
            borderBottomColor: Colors.semiGray,
            marginBottom: 16,
        },
        contentWrapper: {
            flex: 1,
            height: null,
            justifyContent: 'center'
        },

        contentTitleText: {
            ...FontsWeight.Bold,
            color: Colors.primaryColorDark,
            fontSize: 16,
            marginBottom: 4,
        },

        contentTitleTextSub: {
            ...FontsWeight.Regular,
            color: Colors.primaryColor,
            fontSize: 20,
            marginLeft: 16,
            marginBottom: 16
        },

        contentJudulWrapper: {
            flexDirection: 'row',
        },
        headerText: {
            marginBottom: 0,
            marginLeft: 10,
            marginTop: 10
        },
        html: {
            marginTop: 16,
        },
        subHeaderText: {
            fontSize: 24, marginLeft: 20, marginBottom: 5
        },
    }
    // endregion
};

export default Styles;