const Images = {
    logoGas:               require('../../assets/icons/gas.png'),
    logoGerak:             require('../../assets/icons/gerak.png'),
    logoLampu:              require('../../assets/icons/lampu.png'),
    background:             require('../../assets/images/images.jpg'),
}

export default Images;