import {Platform, Dimensions} from 'react-native';
import Colors from "./Colors";

const {width, height} = Dimensions.get('window');

const UIConst = {
    statusBarHeight: Platform.select({
        ios: 20,
        android: 0
    }),
    appBar: Platform.select({
        ios: 0,
        android: 56
    }),
    statusBarStyle: Platform.select({
        ios: 'dark-content',
        android: 'light-content'
    }),
    deviceWidth: width,
    deviceHeight: height,
    shadow4pt: Platform.select({
        ios: {
            shadowColor: Colors.gray,
            shadowOffset: {width: 0, height: 0},
            shadowOpacity: .8,
            shadowRadius: 4
        },
        android: {elevation: 4}
    }),
    shadow2pt: Platform.select({
        ios: {
            shadowColor: Colors.gray,
            shadowOffset: {width: 0, height: 0},
            shadowOpacity: .8,
            shadowRadius: 2
        },
        android: {elevation: 2}
    })
};

export default UIConst;