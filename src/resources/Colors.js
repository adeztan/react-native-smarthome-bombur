const Colors = {
    primaryColor: '#00ab4e',
    primaryColorDark: '#004e44',
    primaryColorLight: '#72bf44',
    secondaryColor: '#bfd730',

    semiWhitePrimaryColor: '#d6ffe9',
    semiTransparentPrimaryColor: 'rgba(8, 127, 35, .8)',

    settingsColor: '#efeff4',

    transparent: 'transparent',
    white: 'white',
    black: 'black',
    semiWhite: '#F7F7F7',
    gray: '#9E9E9E',
    semiGray: '#e1e2e1',
    titleColor: '#737378',

    emas: '#FBC02D',

    merah300: '#E57373',
    merah400: '#EF5350',
    merah500: '#F44336',
    merah700: '#D32F2F',
    merah900: '#B71C1C',
    merahDark: '#4d0000',

    biru300: '#64B5F6',
    biru500: '#2196F3',

    teal300: '#4DB6AC',
    teal500: '#009688',

    hijau300: '#81C784',
    hijau500: '#4CAF50',

    orange300: '#FFB74D',
    orange500: '#FF9800',

    coklat300: '#A1887F',
    coklat500: '#795548',

    lemon300: '#DCE775',
    lemon500: '#CDDC39',

    ungu300: '#BA68C8',
    ungu500: '#9C27B0',

    darkGrey: '#656d87',
    lightBlue: '#79abe4',

    emasDark: '#b27f30',
    coklat2: '#5f3a10',
    coklat3: '#724808'
};

export default Colors;