import React, {Component} from 'react';
import {Dimensions, Image, Text, View} from 'react-native';
import SwitchToggle from 'react-native-switch-toggle';
import {db} from "../../Config/ConfigFirebase";

type Props = {};
export default class Store extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = ({
                suhu: 88,
                gas: 20,
                gerak: 1,
                lampu: false,
            }
        )
    }

    componentDidMount = () => {
        const dataFirebase = db.ref('temp');

        dataFirebase.once('value', snapshot => {
            let data = snapshot.val();
            this.setState({
                ...this.state,
                suhu: data.valSuhu,
                gas: data.valGas,
                gerak: data.valGerak,
                lampu : data.valLampu
            });
        }, () => {

        })
    }
    componentDidUpdate = () => {
        const dataFirebase = db.ref('temp');

        dataFirebase.once('value', snapshot => {
            let data = snapshot.val();
            this.setState({
                ...this.state,
                suhu: data.valSuhu,
                gas: data.valGas,
                gerak: data.valGerak,
                lampu : data.valLampu
            });
        }, () => {

        })
    }
    render() {
        return (
            <View style={{flex: 1}}>
                <View style={{flex: 3}}>
                    <Image source={require('../../../assets/images/images.png')} style={{flex: 1, resizeMode: 'cover', opacity: 0.8}}/>
                    <View style={{position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#ffffff' }}>Smarthome Singkil</Text>
                        <Text style={{ fontSize: 120, fontWeight: 'bold', color: '#ffffff' }}>{Math.ceil(this.state.suhu)}°</Text>
                    </View>
                </View>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#eaeaea', paddingHorizontal: 16}}>
                        <View style={{marginRight: 16}}><Image source={require('../../../assets/icons/gas.png')} style={{aspectRatio: 1/1, height: 32}}/></View>
                        <View style={{flex: 1}}><Text>Gas</Text></View>
                        <View style={{width: 60}}><Text style={{color: '#979797', textAlign: 'right'}}>{this.state.gas} ppm</Text></View>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#eaeaea', paddingHorizontal: 16}}>
                        <View style={{marginRight: 16}}><Image source={require('../../../assets/icons/gerak.png')} style={{aspectRatio: 1/1, height: 32}}/></View>
                        <View style={{flex: 1}}><Text>Gerak</Text></View>
                        <View style={{width: 80}}><Text style={{color: '#979797', textAlign: 'right'}}>{this.state.gerak}</Text></View>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#eaeaea', paddingHorizontal: 16}}>
                        <View style={{marginRight: 16}}><Image source={require('../../../assets/icons/lampu.png')} style={{aspectRatio: 1/1, height: 32}}/></View>
                        <View style={{flex: 1}}><Text>Lampu</Text></View>
                        <View style={{width: 100, alignItems: 'flex-end'}}>
                            <SwitchToggle
                                containerStyle={{
                                    marginTop: 16,
                                    width: 48,
                                    height: 26,
                                    borderRadius: 25,
                                    backgroundColor: '#ccc',
                                    padding: 5,
                                }}
                                circleStyle={{
                                    width: 16,
                                    height: 16,
                                    borderRadius: 8,
                                    backgroundColor: 'white', // rgb(102,134,205)
                                }}
                                switchOn={this.state.lampu}
                                onPress={() => {
                                    this.setState({
                                        ...this.state,
                                        lampu: !this.state.lampu
                                    }, () => {
                                        db.ref('temp').update({
                                            valLampu: this.state.lampu
                                        });

                                    })
                                }}
                                circleColorOff='white'
                                circleColorOn='white'
                                backgroundColorOn='#42b883'
                                duration={500}
                            />
                        </View>
                    </View>

                </View>
            </View>
        );
    }
}
