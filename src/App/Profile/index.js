import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';

export default class Profile extends React.Component {
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.header}></View>
                <Image style={styles.avatar} source={require('../../../assets/images/avatar.jpg')}/>
                <View style={styles.body}>
                    <View style={styles.bodyContent}>
                        <Text style={styles.name}>Fajar Hanggara Pratama</Text>
                        <Text style={styles.info}>Developer</Text>
                        <Text style={styles.description}>
                            Ini adalah aplikasi mobile pertama saya yang telah saya buat. Aplikasi ini ditunjukan untuk rumah yang berada di Perumahan Bumi Singkil Permai Boyolali
                        </Text>
                        <View style={{top: 20}}>
                            <TouchableOpacity style={styles.FacebookStyle} activeOpacity={0.5}>
                                <Image
                                    source={require('../../../assets/images/fb.png')}
                                    style={styles.ImageIconStyle}
                                />
                                <View style={styles.SeparatorLine} />
                                <Text style={styles.TextStyle}> Fajar Hanggara Pratama </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.GooglePlusStyle} activeOpacity={0.5}>
                                <Image
                                    source={require('../../../assets/images/twit.png')}
                                    style={styles.ImageIconStyle}
                                />
                                <View style={styles.SeparatorLine} />
                                <Text style={styles.TextStyle}> @fajarhang</Text>
                            </TouchableOpacity>
                        </View>


                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header:{
        backgroundColor: "#8f1537",
        height:200,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "white",
        marginBottom:10,
        alignSelf:'center',
        position: 'absolute',
        marginTop:130
    },
    name:{
        fontSize:22,
        color:"#FFFFFF",
        fontWeight:'600',
    },
    body:{
        marginTop:40,
    },
    bodyContent: {
        flex: 1,
        alignItems: 'center',
        padding:30,
    },
    name:{
        fontSize:28,
        color: "#696969",
        fontWeight: "600"
    },
    info:{
        fontSize:16,
        color: "#8f1537",
        marginTop:10
    },
    description:{
        fontSize:16,
        color: "#696969",
        marginTop:10,
        textAlign: 'center'
    },
    FacebookStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#485a96',
        borderWidth: 0.5,
        borderColor: '#fff',
        height: 40,
        width: 220,
        borderRadius: 5,
        margin: 5,
    },
    SeparatorLine: {
        backgroundColor: '#fff',
        width: 1,
        height: 40,
    },
    GooglePlusStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#0FA5FF',
        borderWidth: 0.5,
        borderColor: '#fff',
        height: 40,
        width: 220,
        borderRadius: 5,
        margin: 5,
    },
    buttonContainer: {
        marginTop:10,
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom:20,
        width:250,
        borderRadius:30,
        backgroundColor: "#8f1537",
    },
    ImageIconStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
    },
    TextStyle: {
        color: '#fff',
        marginBottom: 4,
        marginRight: 20,
        alignItems: 'center',
        fontWeight : 'bold'
    },
});
