import React from 'react'
import {FlatList, ScrollView, Text, View} from "react-native";
import {db} from "../../Config/ConfigFirebase";
import { SearchBar } from "react-native-elements";
import moment from 'moment';

export default class History extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: [],
            display: [],
            search : '',
            refreshing: false
        }
    }

    componentDidMount = () => {
        this._getData();
    }

    _getData = () => {
        const dataFirebase = db.ref('suhu');

        dataFirebase.once('value', snapshot => {
            let data = snapshot.val();
            let temp = [];
            const entries = Object.keys(data);
            entries.forEach(key => {
                temp.push(data[key]);
            });
            this.setState({
                data: temp,
                display: temp,
                refreshing: false
            });
            console.log(this.state.data);
        }, () => {
            console.log(this.state.data);
        })

    }

    _onSearch = (search) => {
        this.setState({
            ...this.state,
            search: search,
            display: this.state.data.filter((item) => {
                return item.tgl.includes(search);
            })
        });
    }

    _renderItem = data => {
        var { index, item } = data;
        var color = '#42b883';
        if (parseInt(item.val) > 33) {
            color = '#f7be16';
        }
        if (parseInt(item.val) > 37) {
            color = '#ca3e47';
        }
        return (
            <View style={{
                paddingVertical: 15,
                flexDirection: 'row',
                backgroundColor: '#ffffff',
                borderRadius: 5,
                borderLeftWidth: 3,
                borderLeftColor: color,
                marginBottom: 15,
            }} key={ index }>
                <View style={{width: 90, paddingHorizontal: 10, borderRightWidth: 1, borderRightColor: '#d4d7dd'}}>
                    <View><Text style={{textAlign: 'center', color: '#5d5d5d', fontWeight: 'bold', fontSize: 24}}>{moment(item.tgl).format('DD')}</Text></View>
                    <View><Text style={{textAlign: 'center', color: '#979797', fontSize: 12}}>{moment(item.tgl).format('MMM YYYY')}</Text></View>
                </View>
                <View style={{flex: 1, paddingHorizontal: 10}}>
                    <Text style={{fontSize: 18}}>{item.val}°</Text>
                    <Text numberOfLines={1} ellipsizeMode='tail' style={{fontSize: 12}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, aliquid asperiores aspernatur dignissimos dolore ea iusto maxime nisi nulla quasi quo repellat repudiandae sed veritatis voluptatem. Asperiores at, dolorum enim exercitationem numquam odio pariatur ullam unde? Excepturi in magnam obcaecati perferendis quia? Ab adipisci asperiores commodi earum eligendi facilis laborum non, nulla, obcaecati officia quia quis quo quod reprehenderit tempore unde veniam. A accusantium error facilis in magnam, ratione rerum sequi totam voluptate? Animi beatae delectus enim. Autem eaque exercitationem, harum incidunt provident quisquam sed tempora totam voluptatum. Aliquam, architecto commodi cum doloremque ea ex hic impedit libero nemo rem!</Text>
                </View>
            </View>
        )
    }
    render() {

        return (
            <View style={{flex: 1, backgroundColor: '#dedede'}}>
                <SearchBar
                    lightTheme round
                    placeholder="Search"
                    platform="android"
                    onChangeText={this._onSearch}
                    value={this.state.search}
                    cancelButtonText = "Cancel"
                />

                <ScrollView contentContainerStyle={{}}>

                    <View style={{flex: 1, padding: 15}}>
                        <FlatList
                            refreshing={this.state.refreshing}
                            data={this.state.display}
                            renderItem={ this._renderItem }
                            onRefresh={() => {
                                this.setState({
                                    refreshing: true
                                }, () => {
                                    setTimeout(() => {
                                        console.log('on refresh');
                                        this._getData();
                                    }, 1500);

                                });
                            }}
                        />

                    </View>
                </ScrollView>
            </View>
        )
    }
}
